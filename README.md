# Context logging utilities

Small toolset to configure non-blocking logger and json-formatter with optional
fields.

# Why?
It is just a shortcuts for some stuff to use lib not copy/paste.

# Utilities

## Non blocking logger

It is just an example from Python doc but prepared to use for aiohttp.
You need to configure QueueHandler with `context_logging_utility.get_queue_handler`,
it accepts `max_size` arg for Queue.
And to configure handlers for QueueListener you need set up a "fake_logger"
and call `create_listener` to get listener.

```python
from aiohttp import web
from context_logging_utility import FAKE_LOGGER, create_listener


LOGGING = {
    "handlers": {
        "queued": {"()": "context_logging_utility.get_queue_handler"},
    },
    "loggers": {
        FAKE_LOGGER: {"handlers": ["console", "file"]},
    }
}


async def logging_ctx(app: web.Application):
    listener = create_listener(default_logger=f"app_name")
    listener.start()

    yield

    listener.stop()
```

## Context logging

You can use `ContextFormatter` to achieve two possibilities:

- At first, it inherited from `JsonLogger`, so you can write json-logs
- Also, you can use formatter with options fields: if LogRecord object have 
  no such field, then it will not add in log message. 
  Instead of raise AttributeError/KeyError or configure many formatters/handlers/logger.

To add context use `contextualize` function, it wraps logger with LoggerAdapter
    and add function `kwargs` to LoggerAdapter `extra`.

```python
from logging import getLogger
from logging.config import dictConfig
from uuid import uuid4

from context_logging_utility import (
    PLAIN_OUTPUT_FORMAT,
    FAKE_LOGGER,
    create_listener,
)
from context_logging_utility.utils import contextualize


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "plaintext": {
            "()": "context_logging_utility.ContextFormatter",
            "fmt": "%(asctime)s | %(levelname)-8s | %(message)s",
            "context": ["funcName", "pathname", "lineno", "request_id"],
            "separator": " | ",
            "output_format": PLAIN_OUTPUT_FORMAT,
        },
        "json": {
            "()": "context_logging_utility.ContextFormatter",
            "fmt": "%(message)s",
            "context": [
                "asctime",
                "funcName",
                "pathname",
                "lineno",
                "request_id",
            ],
            "rename_fields": {"asctime": "@timestamp"},
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
            "formatter": "plaintext",
        },
        "file": {
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stdout",
            "formatter": "json",
        },
        "queued": {"()": "context_logging_utility.get_queue_handler"},
    },
    "loggers": {
        f"app_name": {"level": "DEBUG", "handlers": ["queued"]},
        FAKE_LOGGER: {"handlers": ["console", "file"]},
    },
}

logger = getLogger("app_name")


def main():
    dictConfig(LOGGING)
    listener = create_listener(default_logger=f"app_name")
    listener.start()

    logger.info("Hello!")

    request_logger = contextualize(logger, request_id=str(uuid4()))
    task_logger = contextualize(logger, task_id=str(uuid4()))

    request_logger.debug("Some request context")
    task_logger.debug("Some task context")
    request_logger.debug("Handle request")
    task_logger.debug("Execute task")

    logger.info("Bye!")

    listener.stop()


if __name__ == "__main__":
    main()
```