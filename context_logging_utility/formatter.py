from datetime import datetime
from logging import LogRecord
from typing import Any

from pythonjsonlogger.jsonlogger import JsonFormatter

from .constants import JSON_OUTPUT_FORMAT


class ContextFormatter(JsonFormatter):
    """
    A formatter for log messages that allows adding optional context fields.

    Args:
        context (list[str], optional): List of context fields to include in log
            messages. Defaults to an empty list.
        output_format (str, optional): The output format for log messages.
            Use 'json' for JSON format and 'plain' for plain text.
            Defaults to 'plain'.
        separator (str, optional): Separator used between the log message and
            optional context fields in plain text format.
            Defaults to a space (' ').
        **kwargs: Additional keyword arguments to pass to the base class
            constructor.

    Attributes:
        context_fields (list): List of context fields to include in log messages.
        output_format (str): The output format for log messages ('json' or 'plain').
        separator (str): Separator used between the log message and optional
            context fields in plain text format.

    Example:
        formatter = ContextFormatter(
            context=['request_id', 'user_id'],
            output_format='json'
        )
        handler.setFormatter(formatter)
    """

    def __init__(
        self,
        *args,
        context: list[str] = None,
        output_format: str = JSON_OUTPUT_FORMAT,
        separator: str = " ",
        **kwargs,
    ):
        self.context_fields = context or []
        self.output_format = output_format
        self.separator = separator

        super().__init__(*args, **kwargs)

    def format(self, record: LogRecord) -> str:
        if self.output_format == JSON_OUTPUT_FORMAT:
            return super().format(record)
        else:
            log_text = super(JsonFormatter, self).format(record)
            add_text = self._add_optional_text(record)

            return log_text + self.separator + add_text

    def _add_optional_text(self, record):
        add_context = []
        for key in self.context_fields:
            try:
                val = getattr(record, key)
            except AttributeError:
                continue

            add_context.append(f"{key}={val}")

        return " ".join(add_context)

    def formatTime(self, record: LogRecord, datefmt=None) -> str:
        return datetime.fromtimestamp(record.created).isoformat()

    def add_fields(
        self,
        log_record: dict[str, Any],
        record: LogRecord,
        message_dict: dict[str, Any],
    ) -> None:
        self._add_optional_fields(log_record, record, message_dict)
        super().add_fields(log_record, record, message_dict)

    def _add_optional_fields(
        self,
        log_record: dict[str, Any],
        record: LogRecord,
        message_dict: dict[str, Any],
    ):
        for key in self.context_fields:
            try:
                val = getattr(record, key)
            except AttributeError:
                continue

            log_record[key] = val
